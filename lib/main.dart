import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simple_gallery/bloc/application_bloc.dart';
import 'package:simple_gallery/pages/main_page.dart';
import 'package:simple_gallery/pages/photo_detail_page.dart';
import 'package:simple_gallery/styles.dart';

void main() {
  return runApp(
    BlocProvider(
      create: (context) => ApplicationBloc()..add(LoadApplication()),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simple Gallery',
      theme: ThemeData(
        primaryColor: CustomColors.primaryColor,
        accentColor: CustomColors.secondaryColor,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MainPage(),
        '/photo_detail': (context) =>
            PhotoDetailPage(photo: ModalRoute.of(context).settings.arguments),
      },
    );
  }
}
