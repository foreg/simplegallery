import 'package:simple_gallery/api/unsplash_api.dart';
import 'package:simple_gallery/models/photo.dart';

class PhotosRepository {
  UnsplashApi _unsplashApi = UnsplashApi();

  Future<List<Photo>> getPhotos(
          {int page = 1, int perPage = 10, String query = 'latest'}) =>
      _unsplashApi.getPhotos(page: page, perPage: perPage, orderBy: query);
}
