import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart';

import 'package:simple_gallery/models/photo.dart';
import 'package:simple_gallery/repository/photos_repository.dart';

part 'photo_event.dart';
part 'photo_state.dart';

enum QueryTypes {
  latest,
  popular
}

class PhotoBloc extends Bloc<PhotoEvent, PhotoState> {
  final PhotosRepository photosRepository;
  final QueryTypes queryType;

  PhotoBloc({this.photosRepository, this.queryType}) : assert(photosRepository != null);

  @override
  PhotoState get initialState => PhotoUninitialized();

  @override
  Stream<PhotoState> transformEvents(Stream<PhotoEvent> events,
      Stream<PhotoState> Function(PhotoEvent event) next) {
    return super.transformEvents(
      events.debounceTime(Duration(milliseconds: 500)),
      next,
    );
  }

  @override
  Stream<PhotoState> mapEventToState(PhotoEvent event) async* {
    final currentState = state;
    if (event is Remove && currentState is PhotoLoaded) {
      final updatedPhotos = List<Photo>.from(currentState.photos)..remove(event.photo);
      yield PhotoLoaded(photos: updatedPhotos);
    }
    else if (event is Fetch) {
      try {
        String query;
        switch (queryType) {
          case QueryTypes.latest:
            query = 'latest';
            break;
          case QueryTypes.popular:
            query = 'popular';
            break;
          default:
            query = 'latest';
        }
        if (currentState is PhotoUninitialized) {
          final photos = await photosRepository.getPhotos(page: 1, query: query);
          yield PhotoLoaded(photos: photos);
        } else if (currentState is PhotoLoaded) {
          final photos = await photosRepository.getPhotos(
              page: currentState.photos.length ~/ 10 + 1, query:query);
          yield PhotoLoaded(photos: currentState.photos + photos);
        }
      } catch (_) {
        yield PhotoError();
      }
    }
  }
}
