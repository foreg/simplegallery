part of 'photo_bloc.dart';

abstract class PhotoEvent extends Equatable {
  const PhotoEvent();

  @override
  List<Object> get props => [];
}

class Fetch extends PhotoEvent {}

class Remove extends PhotoEvent {
  final Photo photo;

  const Remove({this.photo});
}