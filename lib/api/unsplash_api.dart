import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import 'package:simple_gallery/models/photo.dart';

class UnsplashApi {
  final String _host = 'https://api.unsplash.com/photos/';
  final String _clientId = 'WpL_cZzSFlVUwe03D4ZldlyaRzEsCC_o_NdFGuhKmqM';

  //TODO убрать
  Future<List<Photo>> getPhotosMock(
      {int page, int perPage, String query}) async {
    // await Future.delayed(Duration(seconds: 1));
    switch (page) {
      case 1:
        return [
          Photo('1', 'null', 'http://via.placeholder.com/150x150',
              'http://via.placeholder.com/150x150', 'username', UniqueKey()),
          Photo('2', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('3', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('4', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('5', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('6', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('7', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('8', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('9', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('10', 'null', 'http://via.placeholder.com/300x150',
              'http://via.placeholder.com/300x150', 'username', UniqueKey()),
        ];
        break;
      case 2:
        return [
          Photo('11', 'null', 'http://via.placeholder.com/150x150',
              'http://via.placeholder.com/150x150', 'username', UniqueKey()),
          Photo('12', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('13', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('14', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('15', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('16', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('17', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('18', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('19', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('20', 'null', 'http://via.placeholder.com/300x150',
              'http://via.placeholder.com/300x150', 'username', UniqueKey()),
        ];
        break;
      case 3:
        return [
          Photo('21', 'null', 'http://via.placeholder.com/150x150',
              'http://via.placeholder.com/150x150', 'username', UniqueKey()),
          Photo('22', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('23', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('24', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('25', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('26', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('27', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('28', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('29', 'null', 'http://via.placeholder.com/250x250',
              'http://via.placeholder.com/250x250', 'username', UniqueKey()),
          Photo('30', 'null', 'http://via.placeholder.com/300x150',
              'http://via.placeholder.com/300x150', 'username', UniqueKey()),
        ];
        break;
      default:
        return [];
    }
  }

  Future<List<Photo>> getPhotos(
      {int page = 1, int perPage = 10, String orderBy = 'latest'}) async {
    final String query =
        '?client_id=$_clientId&page=$page&per_page=$perPage&order_by=$orderBy';
    final response = await http.get(_host + query);
    List<Photo> photos = new List<Photo>();
    for (var item in json.decode(response.body)) {
      photos.add(Photo.fromJson(item));
    }
    return photos;
  }
}
