import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

class Photo extends Equatable{
  final String id;
  final String description;
  final String thumbUrl;
  final String regularUrl;
  final String username;
  final Key key;

  const Photo(this.id, this.description, this.thumbUrl, this.regularUrl, this.username, this.key);

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(json['id'], 
                json['description'] ?? 'Untitled',
                json['urls']['thumb'] ?? 'http://via.placeholder.com/150x150', 
                json['urls']['regular'] ?? 'http://via.placeholder.com/150x150',
                json['user']['username'] ?? 'Untitled',
                UniqueKey());
  }

  @override
  List<Object> get props => [id, description, thumbUrl, regularUrl, username];
}
