import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simple_gallery/bloc/application_bloc.dart';
import 'package:simple_gallery/bloc/photo_bloc.dart';
import 'package:simple_gallery/pages/splash_screen.dart';
import 'package:simple_gallery/repository/photos_repository.dart';
import 'package:simple_gallery/widgets/custom_drawer.dart';
import 'package:simple_gallery/widgets/photo_gallery.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = [
    Tab(text: 'Latest'),
    Tab(text: 'Popular'),
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ApplicationBloc, ApplicationState>(
      builder: (context, state) {
        if (state is ApplicationInitial) {
          return SplashScreen();
        }
        if (state is ApplicationLoaded) {
          _tabController.animateTo(state.prefs.getInt('defaultTabIndex') ?? 0);
          BlocProvider.of<ApplicationBloc>(context).add(PrepareApplication());
          return SplashScreen();
        }
        if (state is ApplicationReady) {
          return SafeArea(
            child: Scaffold(
              appBar: AppBar(
                title: const Text('Simple Gallery'),
                bottom: TabBar(
                  controller: _tabController,
                  tabs: myTabs,
                ),
              ),
              drawer: CustomDrawer(),
              body: TabBarView(
                controller: _tabController,
                children: [
                  BlocProvider(
                    create: (context) => PhotoBloc(
                        photosRepository: PhotosRepository(),
                        queryType: QueryTypes.latest),
                    child: PhotoGallery(),
                  ),
                  BlocProvider(
                    create: (context) => PhotoBloc(
                        photosRepository: PhotosRepository(),
                        queryType: QueryTypes.popular),
                    child: PhotoGallery(),
                  ),
                ],
              ),
            ),
          );
        }
        return Center(
          child: Text('Unknown error'),
        );
      },
    );
  }
}
