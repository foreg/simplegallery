import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import 'package:simple_gallery/models/photo.dart';

class PhotoDetailPage extends StatelessWidget {
  final Photo photo;

  const PhotoDetailPage({Key key, this.photo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SizedBox.expand(
        child: Hero(
          tag: photo.key,
          flightShuttleBuilder: (
            BuildContext flightContext,
            Animation<double> animation,
            HeroFlightDirection flightDirection,
            BuildContext fromHeroContext,
            BuildContext toHeroContext,
          ) {
            return RotationTransition(
              turns: animation,
              child: (toHeroContext.widget as Hero).child,
            );
          },
          child: CachedNetworkImage(
            fit: BoxFit.fitWidth,
            imageUrl: photo.regularUrl,
            imageBuilder: (context, imageProvider) => PhotoView(
              minScale: PhotoViewComputedScale.contained,
              imageProvider: imageProvider,
            ),
            placeholder: (context, url) => Stack(
              alignment: Alignment.center,
              children: [
                SizedBox.expand(
                  child: CachedNetworkImage(
                    fit: BoxFit.fitWidth,
                    imageUrl: photo.thumbUrl,
                  ),
                ),
                BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 2,
                    sigmaY: 2,
                  ),
                  child: Container(
                    color: Colors.black.withOpacity(0.1),
                  ),
                ),
              ],
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
      ),
    );
  }
}
