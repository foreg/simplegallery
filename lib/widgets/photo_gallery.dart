import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simple_gallery/bloc/photo_bloc.dart';
import 'package:simple_gallery/models/photo.dart';
import 'package:simple_gallery/widgets/preloader.dart';
import 'package:simple_gallery/widgets/photo_card.dart';

class PhotoGallery extends StatefulWidget {

  const PhotoGallery({Key key}) : super(key: key);

  @override
  _PhotoGalleryState createState() => _PhotoGalleryState();
}

class _PhotoGalleryState extends State<PhotoGallery>
    with AutomaticKeepAliveClientMixin {
  @override
  bool wantKeepAlive = true;

  ScrollController _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  PhotoBloc _photoBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
    _photoBloc = BlocProvider.of<PhotoBloc>(context)..add(Fetch());
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _scrollListener() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _photoBloc.add(Fetch());
    }
  }

  _removeItem(Photo photo) {
    _photoBloc.add(Remove(photo: photo));
  }

  _buildItem(Photo photo) {
    return GridTile(
      child: Dismissible(
        key: ValueKey(photo.id),
        onDismissed: (_) => _removeItem(photo),
        background: Container(
          color: Colors.red,
        ),
        child: PhotoCard(photo: photo),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<PhotoBloc, PhotoState>(
      builder: (context, state) {
        if (state is PhotoUninitialized) {
          return Center(
            child: Preloader(),
          );
        }
        if (state is PhotoError) {
          return Center(
            child: Text('failed to fetch photos'),
          );
        }
        if (state is PhotoLoaded) {
          if (state.photos.isEmpty) {
            return Center(
              child: Text('no photos'),
            );
          }
          return GridView.builder(
            controller: _scrollController,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemBuilder: (context, index) => index >= state.photos.length
                ? Preloader()
                : _buildItem(state.photos[index]),
            itemCount: state.photos.length + 2,
          );
        }
        return Center(
          child: Text('Unknown error'),
        );
      },
    );
  }
}
