import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:simple_gallery/models/photo.dart';
import 'package:simple_gallery/styles.dart';
import 'package:simple_gallery/widgets/preloader.dart';

class PhotoCard extends StatelessWidget {
  final Photo photo;

  const PhotoCard({Key key, this.photo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          Navigator.pushNamed(context, '/photo_detail', arguments: photo),
      child: Card(
        elevation: 5,
        child: Stack(
          children: <Widget>[
            SizedBox.expand(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Hero(
                  tag: photo.key,
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageUrl: photo.thumbUrl,
                    placeholder: (context, url) => Preloader(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                verticalDirection: VerticalDirection.up,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: AutoSizeText(
                      'by ${photo.username}',
                      maxLines: 1,
                      style: CustomTextStyles.bodyText18White,
                    ),
                    color: Colors.black38,
                  ),
                  Container(
                    child: AutoSizeText(
                      photo.description,
                      maxLines: 2,
                      style: CustomTextStyles.bodyText18White,
                    ),
                    color: Colors.black38,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
