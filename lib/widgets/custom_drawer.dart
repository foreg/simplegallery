import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:simple_gallery/bloc/application_bloc.dart';
import 'package:simple_gallery/styles.dart';

class CustomDrawer extends StatelessWidget {
  _changeDefaultTab(BuildContext context, ApplicationReady state) async {
    int oldValue = state.prefs.getInt('defaultTabIndex') ?? 0;
    state.prefs.setInt('defaultTabIndex', (oldValue + 1) % 2);
    Navigator.pop(context);
  }

  _clearCache(BuildContext context) {
    final manager = new DefaultCacheManager();
    manager.emptyCache();
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: CustomColors.primaryColor,
            ),
            child: Text(
              'Simple Gallery',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          BlocBuilder<ApplicationBloc, ApplicationState>(
            builder: (context, state) {
              if (state is ApplicationReady) {
                return GestureDetector(
                  onTap: () => _changeDefaultTab(context, state),
                  child: ListTile(
                    leading: Icon(Icons.cached),
                    subtitle: Text('Affects at the next app start'),
                    title: Text('Change default tab'),
                  ),
                );
              }
              return Container();
            },
          ),
          GestureDetector(
            onTap: () => _clearCache(context),
            child: ListTile(
              leading: Icon(Icons.settings),
              subtitle: Text('Delete all cached images'),
              title: Text('Clear cache'),
            ),
          ),
        ],
      ),
    );
  }
}
